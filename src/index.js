import * as OfflinePluginRuntime from 'offline-plugin/runtime';
import 'bootstrap/scss/bootstrap-grid.scss';
import 'animate.css/animate.css';
import 'lightgallery.js/dist/css/lightgallery.css';

import './index.html';
import './products.html';
import './downloads.html';
import './index.scss';


import './scripts/script';

import './scripts/productsPage/chart';
import './scripts/productsPage/products';
import './scripts/productsPage/studio';

import './scripts/downloadsPage/downloads';

import './scripts/licensePage/license';

import './scripts/mainPage/spincrement';
import './scripts/mainPage/main';


import './scripts/about/script';


import './scripts/blog/script';

import './scripts/team/script';


OfflinePluginRuntime.install();
