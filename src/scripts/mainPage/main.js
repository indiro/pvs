$(document).ready(function () {
    new Swiper('.b-solution__slider-container', {
        navigation: {
            nextEl: '.swiper-button-next-solution',
            prevEl: '.swiper-button-prev-solution',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        loop: true,
        autoHeight: false,
        noSwiping: true,
        slidesPerView: 1
    });

    new Swiper('.b-enjoy__slider-container', {
        navigation: {
            nextEl: '.swiper-button-next-enjoy',
            prevEl: '.swiper-button-prev-enjoy',
        },
        autoHeight: false,
        noSwiping: true,
        loop: true,
        breakpoints: {
            // when window width is >= 320px
            320: {
                spaceBetween: 5,
                slidesPerView: 2,
                centeredSlides: true
            },
            // when window width is >= 480px
            480: {
                spaceBetween: 14,
                slidesPerView: 4,
                centeredSlides: false
            },
            // when window width is >= 640px
            800: {
                spaceBetween: 20,
                slidesPerView: 4,
                centeredSlides: false
            }
        }
    });

    new Swiper('.b-examples__slider-container', {
        navigation: {
            nextEl: '.swiper-button-next-examples',
            prevEl: '.swiper-button-prev-examples',
        },
        autoHeight: false,
        noSwiping: true,
        loop: true,
        spaceBetween: 5,
        breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: 2,
                centeredSlides: true
            },
            // when window width is >= 480px
            480: {
                slidesPerView: 4,
                centeredSlides: false
            },
            // when window width is >= 640px
            800: {
                slidesPerView: 4,
                centeredSlides: false
            }
        }
    });

    let show = true;
    const countbox = ".b-enjoy__advantage-count";
    if ($(countbox).length != 0) {
        const updateCount = () => {
            if (!show) return false;
            let wt = $(window).scrollTop();
            let wh = $(window).height();
            let et = $(countbox).offset().top;
            let eh = $(countbox).outerHeight();
            let dh = $(document).height();
            if (wt + wh - 200 >= et || wh + wt == dh || eh + et < wh){
                $(countbox).css('opacity', '1');
                $(countbox).spincrement({
                    thousandSeparator: "",
                    duration: 2000
                });

                show = false;
            }
        }

        $(document).ready(updateCount);
        $(window).on("scroll load resize", updateCount);
    }

    $('.b-questions__item').on('click', function () {
        if ($(this).hasClass('b-questions__item--active')) {
            $('.b-questions__item').removeClass('b-questions__item--active');
        } else {
            $('.b-questions__item').removeClass('b-questions__item--active');
            $(this).addClass('b-questions__item--active');
        }
    });

    $('.b-enjoy__advantage-info').on('mousemove', function () {
        const tooltip = $(this).find('.b-enjoy__advantage-tooltip');
        tooltip.addClass('b-enjoy__advantage-tooltip--active');
    });

    $('.b-enjoy__advantage-tooltip').on('mousemove', function () {
        $(this).addClass('b-enjoy__advantage-tooltip--hovered');
    });

    $('.b-enjoy__advantage-info').on('mouseleave', function () {
        setTimeout(() => {
            const tooltip = $(this).find('.b-enjoy__advantage-tooltip');

            if (!tooltip.hasClass('b-enjoy__advantage-tooltip--hovered')) {
                tooltip.removeClass('b-enjoy__advantage-tooltip--active');
            }
        }, 2000);
    });

    $('.b-enjoy__advantage-tooltip').on('mouseleave', function () {
        $(this).removeClass('b-enjoy__advantage-tooltip--hovered');
        $(this).removeClass('b-enjoy__advantage-tooltip--active');
    });

    $('.b-enjoy__advantage-info').on('click', function (e) {
        e.preventDefault();
    });

});