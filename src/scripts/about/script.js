$(document).ready(function () {
    let swiperRise;
    new Swiper('.b-team__slider-container', {
        navigation: {
            nextEl: '.swiper-button-next-team',
            prevEl: '.swiper-button-prev-team',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        on: {
            slideChange: function ({activeIndex}) {
                if (activeIndex === 1 || activeIndex === 4) {
                    setTimeout(() => {
                        $('.b-team__mission--absolute').removeClass('hide');
                    }, 50)
                } else {
                    setTimeout(() => {
                        $('.b-team__mission--absolute').addClass('hide');
                    }, 50)

                }
            },
        },
        loop: true,
        autoHeight: false,
        noSwiping: true,
        slidesPerView: 1
    });


    swiperRise = new Swiper('.b-rise__slider-container', {
        loop: true,
        autoHeight: false,
        noSwiping: true,
        slidesPerView: 1,
        on: {
            slideChange: function ({activeIndex}) {
                setTimeout(() => {
                    setActive(activeIndex - 1, false);
                })
            },
        },
    });

    new Swiper('.b-artic__slider-container', {
        navigation: {
            nextEl: '.swiper-button-next-artic',
            prevEl: '.swiper-button-prev-artic',
        },
        autoHeight: false,
        noSwiping: true,
        loop: true,
        breakpoints: {
            // when window width is >= 320px
            320: {
                spaceBetween: 11,
                slidesPerView: 2,
                centeredSlides: true
            },
            // when window width is >= 440px
            440: {
                spaceBetween: 15,
                slidesPerView: 2,
                centeredSlides: false
            },
            // when window width is >= 760px
            760: {
                spaceBetween: 15,
                slidesPerView: 3,
                centeredSlides: false
            },
            // when window width is >= 800px
            800: {
                spaceBetween: 20,
                slidesPerView: 3,
                centeredSlides: false
            }
        }
    });

    const progressBlock = $('.b-rise__progress-center');
    const yearsBlock = $('.b-rise__years');
    const years = $('.b-rise__year');
    let yearWidth = $(years).width();
    let center = progressBlock.width() / 2;

    years.map((index, year) => {
        for (let i = 0; i < 4; i++) {
            $(year).prepend('<span></span>');
        }
    })

    setTimeout(() => {
        setActive(0);
        centeredActive();
    })

    $(window).resize(function () {
        yearWidth = $(years).width();
        center = progressBlock.width() / 2;

        setTimeout(() => {
            setActive(0);
            centeredActive();
        })
    });

    yearsBlock.mousedown(function(e) {
        e.preventDefault();
        const translated = getTranslated();
        const X = e.clientX;
        $('body').mousemove(function(elem) {
            elem.preventDefault();
            const move = X - elem.clientX
            updateProgress(translated - move);
            if (move !== 0) {
                changeActive();
            }
        }).mouseup(function(){
            $(this).unbind('mousemove');
            centeredActive();
        });

        years.mouseup(function (e) {
            e.preventDefault();
            const move = X - e.clientX
            if (move === 0) {
                const slide =  e.target.getAttribute("data-index");
                setActive(slide);
            }
        });
    });

    yearsBlock.on('touchstart',function(e) {
        const translated = getTranslated();
        const X = e.touches[0].clientX;
        $('body').on('touchmove',function(elem) {
            const move = X - elem.touches[0].clientX;
            updateProgress(translated - move);
            if (move !== 0) {
                changeActive();
            }
        }).on('touchend', function(){
            $(this).unbind('touchmove');
            centeredActive();
        });
    });

    function setActive(slide, moveSlider = true) {
        if (slide === -1) slide = years.length-1;
        if (slide === years.length) slide = 0;
        const translated = getTranslated();
        years.map((index, year) => {
            if ($(year).hasClass('active') && index !== slide) {
                let diff = (index - slide) * yearWidth;
                updateProgress(translated + diff);
            }
        })
        changeActive(moveSlider);
    }

    function updateProgress(moved) {
        const maxDrive = center;
        const minDrive = center - years.length * yearWidth;

        if (moved > minDrive && moved < maxDrive) {
            $(yearsBlock).css("transform", function(i){
                return 'translateX('+ moved +'px)';
            });
        }
    }

    function centeredActive() {
        const translated = getTranslated();

        years.map((index, year) => {
            if ($(year).hasClass('active')) {
                let leftpadd = translated + (index * yearWidth);
                let diff = center - (yearWidth / 2) - leftpadd;
                updateProgress(translated + diff);
            }
        })
    }

    function changeActive(moveSlider = true) {
        const translated = getTranslated();

        years.map((index, year) => {
            if (center > (translated + (index  * yearWidth)) && center < (translated + ((index + 1)  * yearWidth))) {
                years.removeClass('active');
                $(year).addClass('active');
                if (moveSlider) {
                    swiperRise.slideTo(index + 1);
                }
            }
        })
    }

    function getTranslated() {
        return parseInt(yearsBlock.css('-webkit-transform').split(/[()]/)[1].split(',')[4]);
    }

    $('.b-rise__progress-right').on('click', function () {
        const active = years.map((index, year) => {
            if ($(year).hasClass('active')) {
                return index;
            }
        })
        setActive(active[0] + 1);
    });

    $('.b-rise__progress-left').on('click', function () {
        const active = years.map((index, year) => {
            if ($(year).hasClass('active')) {
                return index;
            }
        })
        setActive(active[0] - 1);
    });

});