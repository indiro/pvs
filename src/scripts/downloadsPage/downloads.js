const links = [
    // linux
    [
        [
            'https://files.viva64.com/pvs-studio-7.11.44138.98-x86_64.rpm',
            'https://files.viva64.com/pvs-studio-7.11.44138.98-amd64.deb',
            'https://files.viva64.com/pvs-studio-7.11.44138.98-x86_64.tgz'
        ],
        [
            'https://files.viva64.com/pvs-studio-dotnet-7.11.44142.396-x86_64.rpm',
            'https://files.viva64.com/pvs-studio-dotnet-7.11.44142.396-amd64.deb',
            'https://files.viva64.com/pvs-studio-dotnet-7.11.44142.396-x86_64.tar.gz'
        ]
    ],

    // macOS
    [
        [
            'https://files.viva64.com/pvs-studio-7.11.44138.98-macos.pkg',
            'https://files.viva64.com/pvs-studio-7.11.44138.98-macos.tgz'
        ],
        [
            'https://files.viva64.com/pvs-studio-dotnet-7.11.44142.396-macos.tar.gz'
        ]
    ],

    // Windows
    [
        [
            'https://files.viva64.com/PVS-Studio_setup.exe'
        ],
        [
            'https://files.viva64.com/PVS-Studio_setup.exe'
        ],
        [
            'https://files.viva64.com/PVS-Studio_setup.exe'
        ]
    ],
];

$(document).ready(function () {
    const sliderDownloadNavs = $('.b-downloads__button');
    let swiperDownloads;

    if (window.innerWidth >= '768') {
        swiperDownloads = new Swiper('.b-downloads__slider-container', {
            on: {
                slideChange: function ({activeIndex}) {
                    sliderDownloadNavs.removeClass('b-downloads__button--active');
                    $(sliderDownloadNavs[activeIndex]).addClass('b-downloads__button--active');
                },
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: false,
            },
            autoHeight: false,
            noSwiping: true,
        });
    } else {
        $('.b-downloads__slider-slide').removeClass('swiper-slide');
        $('.b-downloads__slider-container').removeClass('swiper-container');
        $('.b-downloads__slider-wrapper').removeClass('swiper-wrapper');
    }

    $('.b-downloads__button').on('click', function () {
        sliderDownloadNavs.removeClass('b-downloads__button--active');
        $(this).addClass('b-downloads__button--active');
        swiperDownloads.slideTo($(this).attr('data-slide'));
    });

    $('.b-downloads__next, .b-downloads__prev').on('click', function () {
        if (window.innerWidth < '768') {
            const slide = $(this).attr('data-slide');
            $('.b-downloads__slider-slide').removeClass('b-downloads__slider-slide--mobile-active');
            $(`.b-downloads__slider-slide[data-mobile="${slide}"]`).addClass('b-downloads__slider-slide--mobile-active');
        } else {
            swiperDownloads.slideTo($(this).attr('data-slide'));
        }
    });

    $('.b-downloads__slide-title').on('click', function () {
        $('.b-downloads__slider-slide').removeClass('b-downloads__slider-slide--mobile-active');
        $(this).parent().addClass('b-downloads__slider-slide--mobile-active');
    });

    $(window).resize(function () {
        if (window.innerWidth < '768') {
            $('.b-downloads__slider-slide').removeClass('swiper-slide');
            $('.b-downloads__slider-container').removeClass('swiper-container');
            $('.b-downloads__slider-wrapper').removeClass('swiper-wrapper');
            swiperDownloads?.destroy();
        } else {
            if (swiperDownloads?.destroyed || !swiperDownloads) {
                $('.b-downloads__slider-slide').addClass('swiper-slide');
                $('.b-downloads__slider-container').addClass('swiper-container');
                $('.b-downloads__slider-wrapper').addClass('swiper-wrapper');
                swiperDownloads = new Swiper('.b-downloads__slider-container', {
                    on: {
                        slideChange: function ({activeIndex}) {
                            sliderDownloadNavs.removeClass('b-downloads__button--active');
                            $(sliderDownloadNavs[activeIndex]).addClass('b-downloads__button--active');
                        },
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: false,
                        },
                    },
                    autoHeight: true,
                    noSwiping: true
                });
            }
        }
    });

    new Swiper('.b-embedded__slider-container', {
        navigation: {
            nextEl: '.swiper-button-next-embedded',
            prevEl: '.swiper-button-prev-embedded',
        },
        autoHeight: false,
        noSwiping: true,
        loop: true,
        breakpoints: {
            // when window width is >= 320px
            320: {
                spaceBetween: 5,
                slidesPerView: 2,
                centeredSlides: true
            },
            // when window width is >= 480px
            480: {
                spaceBetween: 14,
                slidesPerView: 4,
                centeredSlides: false
            },
            // when window width is >= 640px
            800: {
                spaceBetween: 20,
                slidesPerView: 4,
                centeredSlides: false
            }
        }
    });

    let platform, language, format;


    $('div[data-select="platform"]').on('click', function () {
        $('div[data-select="platform"]').removeClass('active');
        $('div[data-select="language"]').removeClass('active');
        $('div[data-select="format"]').removeClass('active');
        $(this).addClass('active');
        $('div[data-slide="1"]').removeClass('blocked');
        $('div[data-slide="2"]').addClass('blocked');

        platform = $(this).attr('data-value');
        language = '';
    });

    $('div[data-select="language"]').on('click', function () {
        $('div[data-select="language"]').removeClass('active');
        $('div[data-select="format"]').removeClass('active');
        $(this).addClass('active');
        $('div[data-slide="2"]').removeClass('blocked');
        $('.b-downloads__download').addClass('blocked');

        language = $(this).attr('data-value');

        checkFormats();
    });

    $('div[data-select="format"]').on('click', function () {
        $('div[data-select="format"]').removeClass('active');
        $('.b-downloads__download').removeClass('blocked');
        $(this).addClass('active');

        format = $(this).attr('data-value');

        checkLink();
    });

    function checkFormats() {
        if (language && platform) {
            $(`div[data-select="format"]`).hide();
            $(`div[data-select="format"][data-lang="${language}"][data-platform="${platform}"]`).show();
        }

        if (language == 2) {
            $('.b-downloads__download').hide();
            $('.select-format-name').text('Доступен «Быстрый запуск»');
        } else {
            $('.b-downloads__download').show();
            $('.select-format-name').text('Выберите формат');
        }
    }

    function checkLink() {
        if (language != '' && platform != '' && format != '') {
            try {
                const link = links[platform][language][format];
                $('.b-downloads__download').attr('data-link', link);
            } catch (e) {}
        }
    }

    $('.b-downloads__download').on('click', function () {
        const link = $(this).attr('data-link');
        if (link != '') {
            window.open(link);
        } else {
            return false;
        }
    });

    $('.java-start').on('click', function () {
        window.open('https://www.viva64.com/ru/m/0045/');
    });
});