$(document).ready(function () {
    $('.b-command__open-back').click(function () {
        $(this).closest('.b-command__item').toggleClass('b-command__item--transform');
    });

    $('.b-command__open-front').click(function () {
        $(this).closest('.b-command__item').toggleClass('b-command__item--transform');
    });
});