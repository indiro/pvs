$(document).ready(function () {
    const sliderNavs = $('.b-studio__button');
    let swiper;

    if (window.innerWidth >= '768') {
        swiper = new Swiper('.swiper-container', {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            on: {
                slideChange: function ({activeIndex}) {
                    sliderNavs.removeClass('b-studio__button--active');
                    $(sliderNavs[activeIndex]).addClass('b-studio__button--active');
                },
            },
            autoHeight: false,
            noSwiping: true,
            allowTouchMove: false
        });
    } else {
        $('.b-slider__slide').removeClass('swiper-slide');
        $('.b-slider__container').removeClass('swiper-container');
        $('.b-slider__wrapper').removeClass('swiper-wrapper');
    }

    $('.b-studio__button').on('click', function () {
        sliderNavs.removeClass('b-studio__button--active');
        $(this).addClass('b-studio__button--active');
        swiper.slideTo($(this).attr('data-slide'));
    });

    $('.b-slider__slide-title').on('click', function () {
        $('.b-slider__slide').removeClass('b-slider__slide--mobile-active');
        $(this).parent().addClass('b-slider__slide--mobile-active');
    });

    $(window).resize(function () {
        if (window.innerWidth < '768') {
            $('.b-slider__slide').removeClass('swiper-slide');
            $('.b-slider__container').removeClass('swiper-container');
            $('.b-slider__wrapper').removeClass('swiper-wrapper');
            // swiper?.destroy();
        } else {
            if (swiper?.destroyed || !swiper) {
                $('.b-slider__slide').addClass('swiper-slide');
                $('.b-slider__container').addClass('swiper-container');
                $('.b-slider__wrapper').addClass('swiper-wrapper');
                swiper = new Swiper('.swiper-container', {
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                    on: {
                        slideChange: function ({activeIndex}) {
                            sliderNavs.removeClass('b-studio__button--active');
                            $(sliderNavs[activeIndex]).addClass('b-studio__button--active');
                        },
                    },
                    autoHeight: true,
                    noSwiping: true
                });
            }
        }
    });


    // products forms and elements
    $('.b-select').on('click', function () {
        const value = $(this).attr('data-value');

        if (value != '') {
            $(this).addClass('b-select--confirm');
        } else {
            $(this).removeClass('b-select--confirm');
        }
    });

    $('.b-input').on('input', function () {
        const value = $(this).val();
        console.log(value)
        if (value != '') {
            $(this).parent().addClass('b-input--confirm');
        } else {
            $(this).parent().removeClass('b-input--confirm');
        }
    });

    $('.b-textarea').on('input', function () {
        const value = $(this).val();

        if (value != '') {
            $(this).parent().addClass('b-textarea--confirm');
        } else {
            $(this).parent().removeClass('b-textarea--confirm');
        }
    });


    const links = [
        // linux
        [
            [
                'https://files.viva64.com/pvs-studio-7.08.39765.52-x86_64.rpm',
                'https://files.viva64.com/pvs-studio-7.08.39765.52-amd64.deb',
                'https://files.viva64.com/pvs-studio-7.08.39765.52-x86_64.tgz'
            ],
            [
                'https://files.viva64.com/pvs-studio-dotnet-7.08.39471.189-x86_64.rpm',
                'https://files.viva64.com/pvs-studio-dotnet-7.08.39471.189-amd64.deb',
                'https://files.viva64.com/pvs-studio-dotnet-7.08.39471.189-x86_64.tar.gz'
            ]
        ],

        // macOS
        [
            [
                'https://files.viva64.com/pvs-studio-7.08.39765.52-macos.pkg',
                'https://files.viva64.com/pvs-studio-7.08.39765.52-macos.tgz'
            ],
            [
                'https://files.viva64.com/pvs-studio-dotnet-7.08.39471.189-macos.tar.gz'
            ]
        ],

        // java
        [
            [
                'https://files.viva64.com/PVS-Studio_setup.exe'
            ],
            [
                'https://files.viva64.com/PVS-Studio_setup.exe'
            ],
            [
                'https://files.viva64.com/PVS-Studio_setup.exe'
            ],
        ],
    ];

    $('.b-select').on('click', function () {
        if ($(this).hasClass('b-select--active')) {
            $(this).removeClass('b-select--active');
        } else {
            $('.b-select').removeClass('b-select--active');
            $(this).addClass('b-select--active');
        }
        clearTimeout(timerHide);
    });

    $('.b-select__item').on('click', function () {
        $(this).parent().siblings('span').text($(this).text());
        $(this).parent().parent().attr('data-value', $(this).attr('data-value'));
        clearTimeout(timerHide);
    });

    $('.b-select').on('mousemove', function () {
        if ($(this).hasClass('b-select--active')) {
            clearTimeout(timerHide);
        }
    });

    $('.b-select__list, b-select__item').on('mousemove', function () {
        clearTimeout(timerHide);
    });

    $('.b-select').on('mouseleave', function () {
        if ($(this).hasClass('b-select--active')) {
            removeActive();
        }
    });

    $('.b-select__list').on('mouseleave', function () {
        removeActive();
    });

    let timerHide;

    function removeActive() {
        timerHide = setTimeout(() => {
            if ($('.b-select').hasClass('b-select--active')) {
                $('.b-select').removeClass('b-select--active');
            }
        }, 3000);
    }

    $('.b-select[data-name="language"], .b-select[data-name="platform"]').on('click', function () {
        const lang = $('.b-select[data-name="language"]').attr('data-value');
        const plat = $('.b-select[data-name="platform"]').attr('data-value');

        if (lang == 2 && plat != '') {
            $('.b-slider__form-button--run').show();
        } else {
            $('.b-slider__form-button--run').hide();
        }

        if (lang == 2 && (plat == 0 || plat == 1)) {
            $('.b-select[data-name="format"]').addClass('b-select--inactive');
        } else {
            if (lang != '' && plat != '') {
                $('.b-select[data-name="format"]').removeClass('b-select--inactive');
            }
        }
    });

    $('.b-select[data-name="language"], .b-select[data-name="platform"], .b-select[data-name="format"]').on('click', function () {
        checkDist();
    });

    function dischargePlatform() {
        $('.b-select[data-name="platform"] span').text('--Выберите платформу--');
        $('.b-select[data-name="platform"]').attr('data-value', '');
    }

    function dischargeLanguage() {
        $('.b-select[data-name="language"] span').text('--Выберите язык--');
        $('.b-select[data-name="language"]').attr('data-value', '');
    }

    function dischargeFormat() {
        $('.b-select[data-name="format"] span').text('--Формат дистрибутива--');
        $('.b-select[data-name="format"]').attr('data-value', '');
        $('.b-select[data-name="format"]').removeClass('b-select--confirm');
    }

    $('.b-select[data-name="language"] .b-select__item').on('click', function () {
        dischargeFormat();
        checkDist();
    });

    $('.b-select[data-name="platform"] .b-select__item').on('click', function () {
        dischargeFormat();
        checkDist();
    });

    function checkDist() {
        const lang = $('.b-select[data-name="language"]').attr('data-value');
        const plat = $('.b-select[data-name="platform"]').attr('data-value');
        const format = $('.b-select[data-name="format"]').attr('data-value');

        if (lang != '' && plat != '') {
            $('.b-select[data-name="format"] .b-select__item').hide();
            $('.b-select[data-name="format"] .b-select__list').removeClass('b-select__list--hide');
            $('.b-select__item[data-lang="' + lang + '"][data-platform="' + plat + '"]').show();
        }

        if (lang != '' && plat != '' && format != '') {
            try {
                const link = links[plat][lang][format];
                $('.b-slider__form-button--download').removeClass('b-slider__form-button--inactive').attr('data-link', link);
            } catch (e) {
                $('.b-slider__form-button--download').addClass('b-slider__form-button--inactive');
                $('.b-select[data-name="format"] span').text('--Формат дистрибутива--');
            }
        } else {
            $('.b-slider__form-button--download').addClass('b-slider__form-button--inactive');
            $('.b-select[data-name="format"] span').text('--Формат дистрибутива--');
        }
    }

    $('.b-slider__form-button--download').on('click', function () {
        const link = $(this).attr('data-link');
        if (link != '') {
            window.open(link);
        } else {
            return false;
        }
    });


    // trial form

    $('.b-select[data-form="trial"]').on('click', function () {
        checkTrial();
    });

    $('input[data-form="trial"]').on('input', function () {
        checkTrial();
    });

    function checkTrial() {
        const license = $('.b-select[data-form="trial"]').attr('data-value');
        const email = $('input[data-form="trial"]').val();

        if (license != '' && email != '') {
            $('.b-slider__form-button--trial').removeClass('b-slider__form-button--inactive')
        } else {
            $('.b-slider__form-button--trial').addClass('b-slider__form-button--inactive')
        }
    }

    $('.b-slider__form-button--trial').on('click', function () {
        alert('trial form send');
    });


    // price form

    $('.b-select[data-form="price"]').on('click', function () {
        checkPrice();
    });

    $('input[data-form="price"]').on('input', function () {
        checkPrice();
    });

    $('textarea[data-form="price"]').on('input', function () {
        checkPrice();
    });

    function checkPrice() {
        const currency = $('.b-select[data-form="price"]').attr('data-value');
        const email = $('input[data-form="price"]').val();
        const message = $('textarea[data-form="price"]').val();

        if (currency != '' && email != '' && message != '') {
            $('.b-slider__form-button--price').removeClass('b-slider__form-button--inactive')
        } else {
            $('.b-slider__form-button--price').addClass('b-slider__form-button--inactive')
        }
    }

    $('.b-slider__form-button--price').on('click', function () {
        alert('price form send');
    });
});

