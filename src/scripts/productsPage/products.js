Morris.Area.prototype.fillForSeries = function (i) {
    return '90-#00a3ff-#00c1ff:90-#00c7ff-#00cbff';
}

// data for chart - first, first element hidden
const elementData = [
    {
        y: '2010-10-10'
    },
    {
        y: '2011', a: 50,
        content: '<b>PVS-Studio 8.03 (25 июня 2011)</b><br><a href="#">V1038</a>, <a href="#">V1039</a>, <a href="#">V2543</a>, <a href="#">V2544</a>, <a href="https://www.viva64.com/ru/m/0010/">...</a>'
    },
    {
        y: '2012', a: 120,
        content: '<b>PVS-Studio 8.03 (25 июня 2012)</b><br><a href="#">V1038</a>, <a href="#">V1039</a>, <a href="#">V2543</a>, <a href="#">V2544</a>, <a href="https://www.viva64.com/ru/m/0010/">...</a>'
    },
    {
        y: '2012-06-10', a: 170,
        content: '<b>PVS-Studio 9.03 (10 июня 2012)</b><br><a href="#">V1038</a>, <a href="#">V1039</a>, <a href="#">V2543</a>, <a href="#">V2544</a>, <a href="https://www.viva64.com/ru/m/0010/">...</a>'
    },
    {
        y: '2013', a: 260,
        content: '<b>PVS-Studio 10.03 (25 июня 2013)</b><br><a href="#">V1038</a>, <a href="#">V1039</a>, <a href="#">V2543</a>, <a href="#">V2544</a>, <a href="https://www.viva64.com/ru/m/0010/">...</a>'
    },
    {
        y: '2014', a: 340,
        content: '<b>PVS-Studio 11.03 (25 июня 2014)</b><br><a href="#">V1038</a>, <a href="#">V1039</a>, <a href="#">V2543</a>, <a href="#">V2544</a>, <a href="https://www.viva64.com/ru/m/0010/">...</a>'
    },
    {
        y: '2015', a: 400,
        content: '<b>PVS-Studio 12.03 (25 июня 2015)</b><br><a href="#">V1038</a>, <a href="#">V1039</a>, <a href="#">V2543</a>, <a href="#">V2544</a>, <a href="https://www.viva64.com/ru/m/0010/">...</a>'
    },
    {
        y: '2016', a: 450,
        content: '<b>PVS-Studio 13.03 (1 июня 2016)</b><br><a href="#">V1038</a>, <a href="#">V1039</a>, <a href="#">V2543</a>, <a href="#">V2544</a>, <a href="https://www.viva64.com/ru/m/0010/">...</a>'
    },
    {
        y: '2016-06-30', a: 520,
        content: '<b>PVS-Studio 14.03 (30 июня 2016)</b><br><a href="#">V1038</a>, <a href="#">V1039</a>, <a href="#">V2543</a>, <a href="#">V2544</a>, <a href="https://www.viva64.com/ru/m/0010/">...</a>'
    },
    {
        y: '2017', a: 600,
        content: '<b>PVS-Studio 15.03 (25 июня 2017)</b><br><a href="#">V1038</a>, <a href="#">V1039</a>, <a href="#">V2543</a>, <a href="#">V2543</a>, <a href="#">V2543</a>, <a href="#">V2543</a>, <a href="#">V2544</a>, ...'
    },
    {
        y: '2018', a: 640,
        content: '<b>PVS-Studio 16.03 (25 июня 2018)</b><br><a href="#">V1038</a>, <a href="#">V1039</a>, <a href="#">V2543</a>, <a href="#">V2544</a>, <a href="https://www.viva64.com/ru/m/0010/">...</a>'
    },
    {
        y: '2019', a: 730,
        content: '<b>PVS-Studio 17.03 (25 июня 2019)</b><br><a href="#">V1038</a>, <a href="#">V1039</a>, <a href="#">V2543</a>, <a href="#">V2544</a>, <a href="https://www.viva64.com/ru/m/0010/">...</a>'
    },
    {
        y: '2020', a: 830,
        content: '<b>PVS-Studio 18.03 (25 июня 2020)</b><br><a href="#">V1038</a>, <a href="#">V1039</a>, <a href="#">V2543</a>, <a href="#">V2544</a>, <a href="https://www.viva64.com/ru/m/0010/">...</a>'
    }
];


$(window).resize(function () {
    setTimeout(() => {
        $('.tooltip').remove();
        $('circle').tooltips();
    }, 1300)
});


$(document).ready(function () {
    //chart
    const chart = $('#chart');
    if (Object.keys(chart).length != 0) {
        setTimeout(() => {
            Morris.Area({
                element: 'chart',
                data: elementData,
                xkey: 'y',
                ykeys: ['a'],
                // labels: ['Value'],
                resize: true,
                hideHover: true,
                hoverCallback: function (index, options, content, row) {
                    if (index === 0) return false;
                    var data = options.data[index];
                    return 'this is content';
                },
                numLines: 10,
                gridLineColor: 'white',
                gridStrokeWidth: 1.0,
                gridTextColor: 'white',
                pointSize: 7,
                pointFillColors: ['#414e56'],
                lineWidth: 0
            });
        }, 500);

        setTimeout(() => {
            $('.tooltip').remove();
            $('circle').tooltips();
        }, 1300);
    }


    // advantages
    $('.b-advantages__item').on('click', function () {
        setActive($(this).attr('data-index'));
        $(this).addClass('b-advantages__item--hover');
        clearInterval(timer);
    });

    $('.b-advantages__item').on('mousemove', function () {
        if ($(this).hasClass('b-advantages__item--active')) {
            $(this).addClass('b-advantages__item--hover');
            clearInterval(timer);
        }
    });

    $('.b-advantages__item').on('mouseleave', function () {
        if ($(this).hasClass('b-advantages__item--active')) {
            $(this).removeClass('b-advantages__item--hover');
            goAhead();
        }
    });


    const advantages = $('.b-advantages__item');
    let activeItem = 0;

    let timer;

    function goAhead() {
        timer = setInterval(() => {
            activeItem++;
            setActive(activeItem);
        }, 10000);
    }


    function setActive(index) {
        activeItem = index === advantages.length ? 0 : index;
        advantages.removeClass('b-advantages__item--active');
        $(advantages[activeItem]).addClass('b-advantages__item--active');
    }


    let block_show = null;

    function scrollTracking() {
        var wt = $(window).scrollTop();
        var wh = $(window).height();
        var et = $('.b-advantages').offset().top;
        var eh = $('.b-advantages').outerHeight();

        if (wt + wh >= et && wt + wh - eh * 2 <= et + (wh - eh)) {
            if (block_show == null || block_show == false) {
                setActive(activeItem);
                goAhead();
            }
            block_show = true;
        } else {
            if (block_show == null || block_show == true) {
                advantages.removeClass('b-advantages__item--active');
                clearInterval(timer);
            }
            block_show = false;
        }
    }

    const advant = $('.b-advantages');

    if (advant.length != 0) {
        $(window).scroll(function () {
            scrollTracking();
        });

        $(document).ready(function () {
            scrollTracking();
        });
    }
});


(function ($) {
    $.fn.tooltips = function (el) {

        let $tooltip,
            $body = $('body'),
            $el;

        return this.each(function (i, el) {
            $el = $(el).attr('data-tooltip', i);

            const content = elementData[i].content ? elementData[i].content : '';
            let $tooltip = $('<div class="tooltip" data-tooltip="' + i + '">' + content + '<div class="arrow"></div></div>').appendTo('body');

            let linkPosition = $el.position();

            $tooltip.css({
                top: linkPosition.top + 13,
                left: linkPosition.left - 30
            });

            $el
                .removeAttr('title')

                .hover(function () {
                    $el = $(this);

                    $tooltip = $('div[data-tooltip=' + $el.data('tooltip') + ']');

                    var linkPosition = $el.position();

                    $tooltip.css({
                        top: linkPosition.top,
                    });

                    $('circle').removeClass('hovered');
                    $('.tooltip').removeClass('hovered').removeClass('active').removeClass('out');
                    $tooltip.addClass('active');
                    $el.addClass('hovered');

                }, function () {
                    $el = $(this);
                    $tooltip = $('div[data-tooltip=' + $el.data('tooltip') + ']').addClass('out');

                    setTimeout(function () {
                        if (!$tooltip.hasClass('hovered')) {
                            $tooltip.removeClass('active').removeClass('out');
                            $el.removeClass('hovered');
                        }
                    }, 100);
                });

            $tooltip.hover(function () {
                $tooltip.addClass('hovered')
            }, function () {
                setTimeout(function () {
                    $tooltip.removeClass('active').removeClass('out').removeClass('hovered');
                    $el.removeClass('hovered');
                });
            });
        });
    }
})(jQuery);