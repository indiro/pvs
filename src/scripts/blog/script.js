$(document).ready(function () {
    $('.b-blog__tab-search').on('click', function () {
        $(this).toggleClass('b-blog__tab--active-search');
        $('.b-blog__search').toggle();
    });

    $('.b-blog__sub-form').on('submit', function (e) {
        let error = false;
        e.preventDefault();

        const requiredFields = $(this).find('.required');
        if (requiredFields.length > 0) {
            requiredFields.map((i, field) => {
                const value = $(field).find('input').val();
                if (value === '' || (field.classList.contains('email') && !validateEmail(value))) {
                    $(field).addClass('error');
                    error = true;
                } else {
                    $(field).removeClass('error');
                }
            });
        }

        if (!error) {
            $(this).addClass('b-form--submitted');
            $(this).find('input[type="text"]').val('');
            alert('form submit')
        }
    });
});