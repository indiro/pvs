import WOW from 'wow.js';



window.onload = function () {
    lightGallery(document.getElementById('gallery'), {
        controls: false,
        enableThumbDrag: false,
        enableThumbSwipe: false,
        enableSwipe: false,
        loop: false,
        counter: false,
        swipeThreshold: 99999999999999999999
    });

    document.body.classList.add('loaded_hiding');
    window.setTimeout(function () {
        document.body.classList.add('loaded');
        document.body.classList.remove('loaded_hiding');
    }, 500);

    new WOW({
        boxClass: 'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset: 200,          // distance to the element when triggering the animation (default is 0)
        mobile: true,       // trigger animations on mobile devices (default is true)
        live: true,       // act on asynchronously loaded content (default is true)
        callback: function (box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null,    // optional scroll container selector, otherwise use window,
        resetAnimation: true,     // reset animation on end (default is true)
    }).init();

    const flyElements = $('.b-fly__element');
    const topElementsPositions = [];
    const scrollPosition = $(window).scrollTop();
    const documentHeight = $(document).height();
    const windowHeight = $(window).height();


    /*  animation fly elements

    flyElements.map((index, element) => {
        let elementTop = $(element).position().top;
        let objectPositionTop;
        objectPositionTop = elementTop + (scrollPosition / documentHeight * windowHeight);

        setTimeout(() => {
            $(element).css({
                'top':  `${objectPositionTop}px`
            });
        });

        topElementsPositions.push(elementTop);
    });


    $(window).scroll(function () {
        const scrollPosition = $(window).scrollTop();
        $('.b-fly__element').map((index, element) => {
            let elementTop = topElementsPositions[index];
            let objectPositionTop;

            if (scrollPosition / documentHeight < 1) {
                objectPositionTop = elementTop + (scrollPosition / documentHeight * windowHeight);
            }

            $(element).css({
                'top':  `${objectPositionTop}px`
            });
        });
    });
    */

    if (window.innerWidth >= '768') {
        $('.b-header__menu-item').hover(function () {
            $(this).addClass('b-header__menu-item--active');
        }, function () {
            $(this).removeClass('b-header__menu-item--active');
        });


        const headerHeight = $('.b-header').outerHeight(true);

        $(window).scroll(function () {
            if ($(this).scrollTop() > $('.b-header').height()) {
                if (!$('.b-header').hasClass('b-header--fixed')) {
                    $('.b-header').addClass('b-header--fixed');
                }

                // $('body').css({
                //     'padding-top':  `${headerHeight}px`
                // });
            } else {
                $('.b-header').removeClass('b-header--fixed');
                // $('body').css({
                //     'padding-top':  0
                // });
            }

            if ($(this).scrollTop() > 600) {
                $('.b-totop').addClass('b-totop--visible');
            } else {
                $('.b-totop').removeClass('b-totop--visible');
            }
        })

        $('.b-totop').click(function () {
            $('html, body').animate({scrollTop: 0}, 2000);
        });
    } else {
        $('.b-header__menu-item').on('click', function () {
            if ($(this).hasClass('b-header__menu-item--active')) {
                $(this).removeClass('b-header__menu-item--active');
            } else {
                $('.b-header__menu-item').removeClass('b-header__menu-item--active');
                $(this).addClass('b-header__menu-item--active');
            }
        });

        $('.b-header__menu-mobile').on('click', function () {
            $('.b-header__menu-content').toggleClass('b-header__menu-content--active');
            $(".sandwich").toggleClass("active");
            $(".b-header__menu-content").width($(document).width() / 2);
        });

        $('.b-footer__menu-title').on('click', function () {
            if ($(this).parent().hasClass('b-footer__menu-col--active')) {
                $(this).parent().removeClass('b-footer__menu-col--active');
            } else {
                $('.b-footer__menu-col').removeClass('b-footer__menu-col--active');
                $(this).parent().addClass('b-footer__menu-col--active');
            }
        });

        $(window).scroll(function () {
            if ($('.b-header__menu-content').hasClass('b-header__menu-content--active')) {
                $('.b-header__menu-mobile').click();
            }
        })
    }

    $('.b-trial__option').on('click', function () {
        const value = $(this).attr('data-value');
        $('input[name="license"]').val(value);

        if (!$(this).hasClass('b-trial__option--selected')) {
            $('.b-trial__option').removeClass('b-trial__option--selected');
            $(this).addClass('b-trial__option--selected');
        }
    });

    $('.b-form__select-option').on('click', function () {
        const value = $(this).attr('data-value');
        $(this).parent().parent().find('input').val(value);

        if (!$(this).hasClass('b-form__select-option--selected')) {
            $(this).parent().parent().find('.b-form__select-option').removeClass('b-form__select-option--selected');
            $(this).addClass('b-form__select-option--selected');
        }
    });

    $('.openform').on('click', function (e) {
        e.preventDefault();
        const form = $(this).attr('data-form');
        const license = $(this).attr('data-license');
        if (license && license != '') {
            selectLicense(license);
        }
        openForm(form);
    });

    $('.b-form__close, .b-forms__overlay').on('click', function (e) {
        closeForms();
    });

    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    $('.b-form').on('submit', function (e) {
        let error = false;
        e.preventDefault();

        const requiredFields = $(this).find('.required');
        if (requiredFields.length > 0) {
            requiredFields.map((i, field) => {
               const value = $(field).find('input').val();
               if (value === '' || (field.classList.contains('email') && !validateEmail(value))) {
                   $(field).addClass('error');
                   error = true;
               } else {
                   $(field).removeClass('error');
               }
            });
        }

        if (!error) {
            $(this).addClass('b-form--submitted');
            $(this).find('input[type="text"]').val('');
        }
    });

    $('.b-form__select').on('click', function () {
        if ($(this).hasClass('b-form__select--active')) {
            $(this).removeClass('b-form__select--active');
        } else {
            $('.b-form__select').removeClass('b-form__select--active');
            $(this).addClass('b-form__select--active');
        }
    });

    $('.b-form__select-item').on('click', function () {
        $(this).parent().siblings('span').text($(this).text());
        $(this).parent().siblings('input').val($(this).attr('data-value'));
        $(this).parent().parent().addClass('b-form__select--confirm');
    });

    function selectLicense(license) {
        $(`.b-form__select-option[data-value="${license}"]`).click();
    }

    function openForm(form) {
        if($('form').is(`form[data-form="${form}"]`)) {
            const formElement = $(`form[data-form="${form}"]`);
            formElement.addClass('b-form--opened');
            $('.b-forms').addClass('b-forms--opened');
        }
    }

    function closeForms() {
        $('.b-forms').removeClass('b-forms--opened');
        $('.b-form').removeClass('b-form--opened');
        $('.b-form').removeClass('b-form--submitted');
    }

    $('.b-leftmenu__item span').on('click', function () {
        if ($(this).parent().hasClass('b-leftmenu__item--opened')) {
            $(this).parent().removeClass('b-leftmenu__item--opened');
        } else {
            $(this).parent().addClass('b-leftmenu__item--opened');
        }
    });

    $('.toggle-left-menu').on('click', function () {
        $('.b-leftmenu').toggleClass('b-leftmenu--opened');
    });

    $('.close-left-menu').on('click', function () {
        $('.b-leftmenu').removeClass('b-leftmenu--opened');
    });
}

$(function() {
    let countFiles = 1,
        $body = $('body'),
        typeFileArea = ['txt', 'doc', 'docx', 'ods', 'jpg', 'jpeg', 'png'],
        coutnTypeFiles = typeFileArea.length;

    //create new element
    $body.on('click', '.b-form__file-attach', function() {
        let wrapFiles = $('.b-form__files'),
            newFileInput;

        countFiles = wrapFiles.data('count-files') + 1;
        wrapFiles.data('count-files', countFiles);

        newFileInput = '' +
            '<div class="b-form__file">' +
                '<input class="b-form__file-input" type="file" name="file-' + countFiles + '" id="file-' + countFiles + '">' +
                '<div class="b-form__file-item">' +
                    '<img src="./images/file-logo.png" alt="">' +
                    '<span class="b-form__file-name">Close.png</span>' +
                    '<span class="b-form__file-del">' +
                        '<img src="./images/file-del.png" alt="">' +
                    '</span>' +
                '</div>' +
            '</div>';
        wrapFiles.prepend(newFileInput);

        $(`.b-form__file-input[name="file-${countFiles}"]`).click();
    });

    //show text file and check type file
    $body.on('change', 'input[type="file"]', function() {
        var $this = $(this),
            valText = $this.val(),
            fileName = valText.split(/(\\|\/)/g).pop(),
            fileItem = $this.siblings('.b-form__file-item'),
            beginSlice = fileName.lastIndexOf('.') + 1,
            typeFile = fileName.slice(beginSlice);

        fileItem.find('.b-form__file-name').text(fileName);
        if (valText != '') {
            for (let i = 0; i < coutnTypeFiles; i++) {

                if (typeFile == typeFileArea[i]) {
                    $this.parent().addClass('has-mach');
                }
            }
        } else {
            fileItem.addClass('hide-btn');
        }

        if (!$this.parent().hasClass('has-mach')) {
            $this.parent().addClass('error');
        }

        $this.parent().addClass('b-form__file--show');
    });

    //remove file
    $body.on('click', '.b-form__file-del', function() {
        const elem = $(this).closest('.b-form__file');
        elem.fadeOut(400);
        setTimeout(function() {
            elem.remove();
        }, 400);
    });
});