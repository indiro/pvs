$(document).ready(function () {
    $('.b-licenses__advantage-info').on('mousemove', function () {
        $('.b-licenses__advantage-tooltip').removeClass('b-licenses__advantage-tooltip--hovered');
        $('.b-licenses__advantage-tooltip').removeClass('b-licenses__advantage-tooltip--active');
        const tooltip = $(this).find('.b-licenses__advantage-tooltip');
        tooltip.addClass('b-licenses__advantage-tooltip--active');
    });

    $('.b-licenses__advantage-tooltip').on('mousemove', function () {
        $(this).addClass('b-licenses__advantage-tooltip--hovered');
    });

    $('.b-licenses__advantage-info').on('mouseleave', function () {
        setTimeout(() => {
            const tooltip = $(this).find('.b-licenses__advantage-tooltip');

            if (!tooltip.hasClass('b-licenses__advantage-tooltip--hovered')) {
                tooltip.removeClass('b-licenses__advantage-tooltip--active');
            }
        }, 2000);
    });

    $('.b-licenses__advantage-tooltip').on('mouseleave', function () {
        $(this).removeClass('b-licenses__advantage-tooltip--hovered');
        $(this).removeClass('b-licenses__advantage-tooltip--active');
    });

    $('.b-licenses__advantage-info').on('click', function (e) {
        e.preventDefault();
    });

});